# TASK MANAGER

## DEVELOPER INFO

**NAME**: Maksim Tumanov

**EMAIL**: mytumanov@t1-consulting.ru

**EMAIL**: MYTumanov@yandex.ru

## SOFTWEAR

**JAVA**: OPENJDK 1.8

**OS**: WINDOWS 10 21H2

## HARDWARE

**CPU**: i5

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM 

```
java -jar ./task-manager.jar
```
