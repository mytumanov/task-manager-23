package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;

public interface IProjectService extends IUserOwnerService<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    Project updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    Project changeProjectStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) throws AbstractException;

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException;

}
