package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
