package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show about program";
    }

    @Override
    @NotNull
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Maksim Tumanov");
        System.out.println("e-mail: mytumanov@t1-consulting.ru");
        System.out.println("e-mail: MYTumanov@yandex.ru");
    }

}
