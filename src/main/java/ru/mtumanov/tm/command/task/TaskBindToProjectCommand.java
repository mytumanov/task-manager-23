package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Bind task to project";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
