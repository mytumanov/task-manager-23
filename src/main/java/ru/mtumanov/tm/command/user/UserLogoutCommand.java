package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "logout current user";
    }

    @Override
    @NotNull
    public String getName() {
        return "logout";
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
