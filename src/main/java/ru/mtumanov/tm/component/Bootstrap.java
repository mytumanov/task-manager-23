package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.*;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.command.project.*;
import ru.mtumanov.tm.command.system.*;
import ru.mtumanov.tm.command.task.*;
import ru.mtumanov.tm.command.user.*;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.ArgumentNotSupportedException;
import ru.mtumanov.tm.exception.system.CommandNotSupportedException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.repository.UserRepository;
import ru.mtumanov.tm.service.*;
import ru.mtumanov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new SystemInfoCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommnd());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUpdateById());
        registry(new TaskUpdateByIndex());

        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    public void start(@Nullable final String[] args) {
        initLogger();
        initDemo();
        processArguments(args);
        while (Thread.currentThread().isAlive()) {
            System.out.println("ENTER COMMAND:");
            try {
                @NotNull final String cmd = TerminalUtil.nextLine();
                processCommand(cmd);
                System.out.println("OK");
                loggerService.command(cmd);
            } catch (final Exception e) {
                System.out.println("FAIL");
                loggerService.error(e);
            }
        }
    }

    private void initDemo() {
        try {

            @NotNull final Project p1 = new Project("Test Project", "Project for test", Status.IN_PROGRESS);
            @NotNull final Project p2 = new Project("Complete Project", "Project with complete status", Status.COMPLETED);
            @NotNull final Project p3 = new Project("FKSKDJ#&$&*@(!!111", "Strange project", Status.NOT_STARTED);
            @NotNull final Project p4 = new Project("11112222", "Project with numbers", Status.IN_PROGRESS);

            @NotNull final Task t1 = new Task("NAME1", "description for task");

            projectService.add(p1);
            projectService.add(p2);
            projectService.add(p3);
            projectService.add(p4);

            taskService.add(t1);
            taskService.add(new Task("NAME2", "description2"));
            taskService.add(new Task("NAME3", "description3"));

            userService.create("COOL_USER", "cool", Role.ADMIN);
            @NotNull final User u2 = userService.create("NOT_COOL_USER", "Not Cool", "notcool@email.ru");
            userService.create("123qwe!", "vfda4432!", "vfda4432@email.ru");

            p1.setUserId(u2.getId());
            p2.setUserId(u2.getId());
            t1.setUserId(u2.getId());

        } catch (final AbstractException e) {
            System.out.println("ERROR! Fail to initialize test data.");
            System.out.println(e.getMessage());
        }
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0 || args[0] == null)
            return;
        try {
            processArgument(args[0]);
            System.exit(0);
        } catch (final AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void processArgument(@NotNull String arg) throws AbstractException {
        if (arg.isEmpty())
            return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(@NotNull final String cmd) throws AbstractException {
        if (cmd.isEmpty())
            return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(cmd);
        if (abstractCommand == null)
            throw new CommandNotSupportedException(cmd);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        Runtime.getRuntime()
                .addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

}
