package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IAuthService;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.AccessDeniedException;
import ru.mtumanov.tm.exception.user.AuthenticationException;
import ru.mtumanov.tm.exception.user.PermissionException;
import ru.mtumanov.tm.exception.user.UserNotFoundException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @Override
    @NotNull
    public User registry(@NotNull String login, @NotNull String password, @NotNull String email)
            throws AbstractException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@NotNull String login, @NotNull String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user;
        try {
            user = userService.findByLogin(login);
        } catch (UserNotFoundException e) {
            throw new AccessDeniedException();
        }
        if (user.getPasswordHash() == null)
            throw new AccessDeniedException();
        if (user.isLocked())
            throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(password);
        if (!user.getPasswordHash().equals(hash))
            throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    @Nullable
    public String getUserId() throws AbstractException {
        if (!isAuth())
            throw new AccessDeniedException();
        return userId;
    }

    @Override
    @NotNull
    public User getUser() throws AbstractException {
        if (!isAuth())
            throw new AccessDeniedException();
        if (userId == null)
            throw new AccessDeniedException();
        @NotNull final User user = userService.findOneById(userId);
        return user;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) throws AbstractException {
        if (roles == null)
            return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole)
            throw new PermissionException();
    }

}
