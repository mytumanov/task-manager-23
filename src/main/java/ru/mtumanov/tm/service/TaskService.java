package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.IndexIncorectException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    @NotNull
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description) throws AbstractException {
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        repository.add(userId, task);
        return task;
    }

    @Override
    @NotNull
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description) throws AbstractException {
        if (index < 0)
            throw new IndexIncorectException();
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status) throws AbstractException {
        if (index < 0)
            throw new IndexIncorectException();
        if (index > repository.getSize())
            throw new IndexOutOfBoundsException();
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws AbstractException {
        if (projectId.isEmpty())
            return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}
