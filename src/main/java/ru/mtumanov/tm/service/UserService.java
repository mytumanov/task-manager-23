package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository) {
        super(userRepository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    @NotNull
    public User create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return repository.add(user);
    }

    @Override
    @NotNull
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        if (email.isEmpty())
            throw new EmailEmptyException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @NotNull
    public User removeByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        taskRepository.clear(user.getId());
        projectRepository.clear(user.getId());
        return repository.remove(user);
    }

    @Override
    @NotNull
    public User removeByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        @NotNull final User user = repository.findByEmail(email);
        taskRepository.clear(user.getId());
        projectRepository.clear(user.getId());
        return repository.remove(user);
    }

    @Override
    @NotNull
    public User setPassword(@NotNull final String id, @NotNull final String password) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user = repository.findOneById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    @NotNull
    public User userUpdate(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        @NotNull final User user = repository.findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty())
            return false;
        return repository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty())
            return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        user.setLocked(false);
    }

}
