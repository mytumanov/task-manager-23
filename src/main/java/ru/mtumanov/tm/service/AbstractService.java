package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IRepository;
import ru.mtumanov.tm.api.service.IService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.IndexIncorectException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public M add(@NotNull final M model) {
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null)
            return findAll();
        return repository.findAll(comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull String id) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @NotNull
    public M findOneByIndex(@NotNull Integer index) throws AbstractException {
        if (index < 0)
            throw new IndexIncorectException();
        return repository.findOneByIndex(index);
    }

    @Override
    @NotNull
    public M remove(@NotNull M model) {
        return repository.remove(model);
    }

    @Override
    @NotNull
    public M removeById(@NotNull String id) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull Integer index) throws AbstractException {
        if (index < 0)
            throw new IndexIncorectException();
        return repository.removeByIndex(index);
    }

}
