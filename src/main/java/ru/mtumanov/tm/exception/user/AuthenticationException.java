package ru.mtumanov.tm.exception.user;

public class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("ERROR! User is locked!");
    }

}
